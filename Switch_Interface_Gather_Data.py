# Section 1: Import libraries
import csv
import re
from netmiko import ConnectHandler
from pyoui import OUI

# Section 2: Prompt user for device IP, username, and password
ip_address = input("Enter the IP address of the device: ")
username = input("Enter the username: ")
password = input("Enter the password: ")

# Section 3: Create a dictionary of device information
device = {
    'host': ip_address,
    'username': username,
    'password': password,
    'device_type': 'cisco_ios'
}

# Section 4: Connect to the device
net_connect = ConnectHandler(**device)

# Section 5: Send command to the device
output = net_connect.send_command("show interfaces")
output_mac = net_connect.send_command("show mac address-table")

# Section 6: Close the connection
net_connect.disconnect()

# Section 7: Initialize list to store interface information
interfaces = []

# Section 8: Create a dictionary to store mac addresses
mac_address_dict = {}
for line_mac in output_mac.split("\n"):
    match = re.match(r"(\S+)\s+(\S+)\s+(\S+)", line_mac)
    if match:
        mac_address_dict[match.group(2)] = match.group(3)

# Section 9: Iterate through the list of lines
for line in output.split("\n"):
    match = re.match(r"Ethernet(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)", line)
    if match:
        # Extract the interface name, status, and speed
        interface = match.group(1)
        status = match.group(2)
        speed = match.group(4)
        # Extract the duplex information
        duplex_index = line.find("duplex")
        if duplex_index != -1:
            duplex = line[duplex_index+7:]
        else:
            duplex = "unknown"
        # Extract the mode and VLAN information
        mode_index = line.find("mode")
        if mode_index != -1:
            if "access" in line:
                mode = "access"
                vlan = line[mode_index+5:]
            elif "trunk" in line:
                mode = "trunk"
                vlans = line[mode_index+5:]
            else:
                mode = "unknown"
                vlans = "unknown"
        else:
            mode = "unknown"
            vlans = "unknown"
        # Extract MAC address and vendor information
        if interface in mac_address_dict:
            mac_address = mac_address_dict[interface]
            vendor = OUI(mac_address).info.org
        else:
            mac_address = "unknown"
            vendor = "unknown"
        # Append the interface information to the list
        interfaces.append([interface, status, mode, vlans, speed, duplex, mac_address, vendor])

# Open a CSV file for writing
with open("interface_information.csv", "w") as csvfile:
    # Create a CSV writer
    writer = csv.writer(csvfile)
    # Write the header row
    writer.writerow(["Interface", "Status", "Mode", "VLANs", "Speed", "Duplex", "MAC Address", "Vendor"])
    # Write the data rows
    for interface in interfaces:
        writer.writerow(interface)
    print("Interface information written to interface_information.csv")


